/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shape;

/**
 *
 * @author nonta
 */
public class Rectangle {
    private double w ;
    private double l ;
    public Rectangle(double w,double l){
    this.w = w;
    this.l = l;
    
}
    public double calArea(){
    return w*l;
}
    public double getW(){
    return w;
}
    public void setW(double w){
    if(w<=0){
        System.out.println("Error: Radius must more than zero!!!");
             return;
    }
    this.w = w;
}
    public double getL(){
    return l;
}
    public void setL(double l){
    if(l<=0){
        System.out.println("Error: Radius must more than zero!!!");
             return;
    }
    this.l = l;
    }
}

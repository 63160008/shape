/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shape;

/**
 *
 * @author nonta
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(5,5);
        System.out.println("Area of Square(s1 = "+ square1.getS1()+ " and s2 = "+square1.getS2()+") is "+square1.calArea());
        square1.setS1(6);
        square1.setS2(6);
        System.out.println("Area of Square(s1 = "+ square1.getS1()+ " and s2 = "+square1.getS2()+") is "+square1.calArea());
        square1.setS1(0);
        square1.setS2(0);
        System.out.println("Area of Square(s1 = "+ square1.getS1()+ " and s2 = "+square1.getS2()+") is "+square1.calArea());
    }
}

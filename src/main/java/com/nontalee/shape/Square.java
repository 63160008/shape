/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shape;

/**
 *
 * @author nonta
 */
public class Square {
    private double s1;
    private double s2;
    public Square(double s1,double s2){
        this.s1 = s1;
        this.s2 = s2;
    }
    public double calArea(){
    return s1*s2;
}
    public double getS1(){
        return s1;
    }
    public void setS1(double s1){
        if(s1<=0){
            System.out.println("Error: S1 must more than zero!!!");
            return;
        }
        this.s1 = s1;
    }
    public double getS2(){
        return s2;
    }
    public void setS2(double s2){
        if(s2<=0){
            System.out.println("Error: S2 must more than zero!!!");
            return;
        }
        this.s2 = s2;
    }
 }
